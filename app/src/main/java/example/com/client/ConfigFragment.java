package example.com.client;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.alicecallsbob.fcsdk.android.aed.AED;
import com.alicecallsbob.fcsdk.android.aed.Topic;
import com.alicecallsbob.fcsdk.android.uc.UC;
import com.alicecallsbob.fcsdk.android.uc.UCFactory;
import com.alicecallsbob.fcsdk.android.uc.UCListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Nathan on 07/04/2016.
 */
public class ConfigFragment extends Fragment {

    // declared as a static variable so that we can reference from anywhere in the app
    public static UC UC = null;
    private BrowserFragment browserFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflate the correct view
        final View view = inflater.inflate(R.layout.fragment_config, container, false);

        // turn off the progress bar visibility
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar.setVisibility(View.GONE);

        // setup the button click listener
        final Button startStopButton = (Button) view.findViewById(R.id.start_stop);
        startStopButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (ConfigFragment.UC == null) {
                    // hide the keyboard
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                    // start the task to poll the server for the session ID, and
                    EditText srvrText = (EditText) view.findViewById(R.id.server);
                    new LoginThread(srvrText.getText().toString()).start();

                    // update the UI to indicate we're attempting a potentially long running task
                    // make the buttons and fields disabled
                    startStopButton.setEnabled(false);
                    srvrText.setEnabled(false);

                    // make the progress dial visible
                    ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progress);
                    progressBar.setVisibility(View.VISIBLE);
                }
                else {
                    // UC was initialised - so stop the session
                    ConfigFragment.UC.stopSession();

                    // note - we are not starting this thread - rather, we just
                    // want a shortcut to the method that will update the UI
                    new LoginThread(null).updateUI(false);
                }
            }
        });

        // return the view we've constructed
        return view;
    }

    public void setBrowserFragment(BrowserFragment browserFragment) {
        this.browserFragment = browserFragment;
    }


    // private class for handling the login operation
    private class LoginThread extends Thread implements UCListener {

        private String serverIp;

        // the session ID - sent through from the server
        private String id;

        public LoginThread (String serverIp) {
            this.serverIp = serverIp;
        }

        @Override
        public void run() {
            try {
                String url = "http://" + serverIp + "/id.php";

                // perform the HTTP fetch and get a reader
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
                InputStreamReader is = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader reader = new BufferedReader(is);

                // in our case, there is only 1 line to be read
                this.id = reader.readLine();

                // now we've got the session ID, initialise the UC object
                ConfigFragment.UC = UCFactory.createUc(getActivity(), this.id, this);

                // attempt to start the session
                ConfigFragment.UC.setNetworkReachable(true);
                ConfigFragment.UC.startSession();

            } catch (Exception e) {
                Log.e("Login", "Failed to Login", e);
                this.updateUI(false);
            }
        }

        private void updateUI (final boolean registered) {
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    // set the button label
                    Button startStopButton = (Button) getActivity().findViewById(R.id.start_stop);
                    String label = registered
                            ? "Stop"
                            : "Start";

                    startStopButton.setText(label);

                    // find the UI views
                    EditText serverEditText = (EditText) getActivity().findViewById(R.id.server);
                    ProgressBar progressBar = (ProgressBar) getActivity().findViewById(R.id.progress);

                    // enable / disable as appropriate
                    startStopButton.setEnabled(true);
                    progressBar.setVisibility(View.GONE);
                    serverEditText.setEnabled(!registered);

                    // pop a toast to give some feedback
                    String message = (registered) ? "Registered with server" : "Failed to register";
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                    // reset the UC so that we can re-init if we need
                    if (!registered)
                        ConfigFragment.UC = null;
                }
            });
        }



        /////////////////////////////////////////////////////
        //
        // UCListener methods

        @Override
        public void onSessionStarted() {
            this.updateUI(true);

            this.updateUI(true);

            // now connect to the topic
            AED aed = ConfigFragment.UC.getAED();
            EditText topicText = (EditText) getView().findViewById(R.id.topic);
            String topicString = topicText.getEditableText().toString();
            Topic topic = aed.createTopic(topicString, browserFragment);

            // (creating the topic also triggers connecting to it)
        }

        @Override
        public void onSessionNotStarted() {
            this.updateUI(false);
        }

        @Override
        public void onSystemFailure() {
            this.updateUI(false);
        }

        @Override
        public void onConnectionLost() {
            this.updateUI(false);
        }

        @Override
        public void onConnectionRetry(int attempt, long delayUntilNextRetry) {
            // nothing to do...
        }

        @Override
        public void onConnectionReestablished() {
            this.updateUI(true);
        }

        @Override
        public void onGenericError(String s, String s1) {
            // nothing to do...
        }
    }
}
