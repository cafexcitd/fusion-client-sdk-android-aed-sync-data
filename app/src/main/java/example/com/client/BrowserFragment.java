package example.com.client;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alicecallsbob.fcsdk.android.aed.Topic;
import com.alicecallsbob.fcsdk.android.aed.TopicListener;

public class BrowserFragment extends Fragment implements TopicListener {

    public static final String DATA_KEY = "url";
    private Topic topic;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_browser, container, false);

        // make the log scrollable & clear the lorem ipsum text
        TextView logView = (TextView) rootView.findViewById(R.id.log);
        logView.setMovementMethod(new ScrollingMovementMethod());

        // ensure that the webview will handle URL loads by setting
        // the WebViewClient - this is implemented as a private class
        WebView webView = (WebView) rootView.findViewById(R.id.webview);
        webView.setWebViewClient(new MyWebViewClient());

        // hook up the handler for the go button
        Button goButton = (Button) rootView.findViewById(R.id.go);
        goButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);

                // in this app, I'd prefer the web view to be updated
                // only when I know that the data has been published to the
                // AED - another implementation that prefers a snappier
                // user experience may update the local view immediately,
                // rather than waiting for the network callback to say
                // that the data had been submitted

                // get hold of the Topic, and submit the data
                EditText urlText = (EditText)getView().findViewById(R.id.url);
                String url = urlText.getText().toString();
                if (!url.startsWith("http://"))
                    url = "http://" + url;
                topic.submitData(BrowserFragment.DATA_KEY, url);
            }
        });

        // hook up the handler for the message send button
        Button sendButton = (Button) rootView.findViewById(R.id.send);
        sendButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // hide the keyboard
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);

                // in this app, I'd prefer the message log (TextView) to
                // be updated only when I know that the data has been
                // published to the Topic - another implementation that
                // prefers a snappier user experience may update the local
                // view immediately, rather than waiting for the network
                // callback to say that the data had been submitted

                // get hold of the Topic, and send the message
                EditText messageText = (EditText) rootView.findViewById(R.id.message);
                String message = messageText.getText().toString();
                topic.sendAedMessage(message);
            }
        });

        return rootView;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                TextView textView = (TextView) getView().findViewById(R.id.log);
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    // pops a Toast on the main UI thread
    protected void popToastOnUiThread(final String message) {

        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // updates the webView to the given URL
    protected void updateWebView(final String url) {

        // queue the action back to the UI Thread
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                WebView webView = (WebView) getView().findViewById(R.id.webview);
                webView.loadUrl(url);
            }
        });
    }

    // a simple WebViewClient that causes the WebView to load the intended page
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return false;
        }
    }

    ////////////////////////////////////////////////
    //
    // TopicListener methods

    @Override
    public void onDataDeleted(Topic topic, String key, int version) {
        // this should not be called in this implementation since the UI
        // doesn't expose a method through which the client can delete
        // data - only update it
        popToastOnUiThread("onDataDeleted - should not be called!");
    }

    @Override
    public void onDataNotDeleted(Topic topic, String key, String value) {
        // this should not be called in this implementation
        popToastOnUiThread("onDataNotDeleted - should not be called!");
    }

    @Override
    public void onMessageReceived(Topic topic, String message) {
        // if a message has been received, then just add it to the UI log
        Log.i("test", "Message received: " + message);
        this.addMessageToLog(message);
    }

    @Override
    public void onTopicSent(Topic topic, String message) {
        // the message has been sent successfully - update the UI!
        this.popToastOnUiThread("Message sent");

        // no need to update the local UI yet - we'd expect to get
        // a callback indicating that the message was sent OK
    }

    @Override
    public void onTopicNotSent(Topic topic, String message, String error) {
        this.popToastOnUiThread("onTopicNotSent: " + message + ", " + error);
    }

    @Override
    public void onTopicConnected(Topic topic, Map<String, Object> data) {
        this.topic = topic;

        // if the app's implementation allowed for the listener instance to
        // be connected to multiple Topics, then we'd want to check the topic
        // that this callback relates to by evaluating the Topic parameter
        // passed to this method.  In this implementation, that is not a
        // possibility.

        // iterate over the extracted Topic data
        List<Map<String, Object>> items = (List<Map<String, Object>>) data.get("data");
        for (Map<String, Object> item : items) {

            // examine the current
            String key = (String) item.get("key");
            String value = (String) item.get("value");
            Boolean deleted = (Boolean) item.get("deleted");
            Integer version = (Integer) item.get("version");

            // the only data item that this app is interested in is keyed
            // against the 'page' key, statically declared in this class
            if (key.equals(BrowserFragment.DATA_KEY) && !deleted) {
                // this item is the one that we're interested in - set the browser page
                this.updateWebView(value);
            }
        }
    }

    @Override
    public void onTopicDeleted(Topic topic, String message) {
        // this should not be called in this implementation
        popToastOnUiThread("onTopicDeleted - should not be called!");
    }

    @Override
    public void onTopicDeletedRemotely(Topic topic) {
        popToastOnUiThread("onTopicDeletedRemotely: " + topic.getName());
    }

    @Override
    public void onTopicNotConnected(Topic topic, String message) {
        popToastOnUiThread("onTopicNotConnected: " + message);
    }

    @Override
    public void onTopicNotDeleted(Topic topic, String message) {
        // this should not be called in this implementation
        popToastOnUiThread("onTopicNotDeleted - should not be called!");
    }

    @Override
    public void onTopicNotSubmitted(Topic topic, String key, String value, String message) {
        popToastOnUiThread("onTopicNotSubmitted: " + key + ", " + value + ", " + message);
    }

    @Override
    public void onTopicSubmitted(Topic topic, String key, String value, int version) {
        // successfully submitted to the topic
        this.popToastOnUiThread("Submitted");
    }

    @Override
    public void onTopicUpdated(Topic topic, String key, String value, int version, boolean deleted) {
        // some other client submitted data to the topic - update the webview
        if (key.equals(BrowserFragment.DATA_KEY) && !deleted)
            this.updateWebView(value);
        else
            this.updateWebView("about:blank");
    }
}
